--------------------------------------------------------------------------------
                             Backend Entity Search
--------------------------------------------------------------------------------

Maintainers:
 * Jakob Lau Nielsen (frakke), frakke2000@hotmail.com

The Entity Backend Search module allows developers and site admins to quickly
set up highly customizable search pages for the editorial users on a site.

This module has a lot more filter options than those often supplied by the
modules, that entity types comes with.

Sit together with the client and configure it live or let the more technical
client do the configuration.

Project homepage: http://drupal.org/project/entity_backend_search


Installation and configuration
------------------------------

* Just enable the module under "Fields". It only has a few dependencies to the
  core modules, but you should have those enabled by default.

* You are almost done. The rest is basic configurations...

* Naviate to the configuration page found by the module or at
  /admin/config/search/entity_backend_search

* There you will see a list of entity types on your site. Click edit on one of
  them.

* Check the enable button, set up permissions, and now you can focus on the
  filter options and result table displayed to the editorial user.

* Choose filters. Enable filters by choosing another filter option than
  "Disabled" ;-). Order the filters by dragging them and perhaps group the
  filters in fieldsets by entering a group name.

* Choose search result options. Choose how may results to display per page and
  read about the pager.

* The result table.
  For user and node pages, a result table is formatted in code, but you might
  enable another entity type, so you have to either defined one in code - or
  just configure one here.
  The fields are based on the columns available on the base table for the
  entity.
    - Enable them,
    - drag them to re-order to columns,
    - give them a more pretty name in the table header,
    - choose a value formatter
    - or input a pattern with text replacements as described above the table,
    - enable sort on the column.

* In most cases: Done.
  Some times you have more specific needs which can't be met by this backend.
  Take a look at the .api file or simply check out the various implementations
  under the /modules directory in the module. Chances are, that you can get what
  you need with a few simple lines of code.
