<?php
/**
 * @file
 * Entity Search integration for the user module.
 */

// -----------------------------------------------------------------------------
// Table cell formatter.
/**
 * Implements hook_entity_backend_search_table_field_formaters_list_alter().
 */
function user_entity_backend_search_table_field_formaters_list_alter(&$formatters, $context) {
  if ($context['field_name'] == 'uid' && $context['type'] == 'int') {
    $formatters['account'] = t('Link to user');
  }
}

/**
 * Implements hook_entity_backend_search_table_field_formaters_render_alter().
 */
function user_entity_backend_search_table_field_formaters_render_alter(&$field_value, $context) {
  if ($context['formatter'] == 'account') {
    if ($account = user_load($field_value)) {
      $field_value = l($account->name, 'user/' . $account->uid);
    }
  }
}

// -----------------------------------------------------------------------------
// Filter fields.
/**
 * Implements hook_entity_backend_search_filter_fields_alter().
 */
function user_entity_backend_search_filter_fields_alter(&$fields, $entity_type) {
  if ($entity_type != 'user') {
    return;
  }

  $fields['user_role_id'] = array(
    '#type' => 'fieldset',
    'label' => array(
      '#markup' => t('User roles'),
    ),
    'instances' => array(
      '#markup' => t('All'),
    ),
  );
}

/**
 * Implements hook_entity_backend_search_filter_types().
 */
function user_entity_backend_search_filter_types($entity_type, $field_name) {
  if ($field_name == 'user_role_id') {
    $filters = array(
      'user_role_id' => t('User role ids'),
    );
    return $filters;
  }
}

/**
 * Implements hook_entity_backend_search_field_widget().
 */
function user_entity_backend_search_field_widget($entity_type, $field_name, $field_settings, $default_values) {
  if ($entity_type != 'user') {
    return;
  }

  $field_widget = FALSE;

  if ($field_name == 'user_role_id') {
    $field_widget = array(
      '#type' => 'select',
      '#multiple' => TRUE,
      '#title' => t('Roles'),
      '#description' => t('User has at least one of the selected roles.'),
      '#options' => user_roles(TRUE),
      '#default_value' => isset($default_values['user_role_id']) ? $default_values['user_role_id'] : array(),
    );
  }

  return $field_widget;
}

/**
 * Implements hook_entity_backend_search_field_widget_alter().
 */
function user_entity_backend_search_field_widget_alter(&$field_widget, $context) {
  // General alterations.
  if ($context['field_name'] == 'uid' && $context['field_settings']['filter_widget_types'] == 'textfield_autocomplete') {
    // Add autocomplete for user. The entity backend search autocomplete is used
    // rahter than user/autocomplete to support multiple user searches.
    $field_widget['#title'] = t('Username');
    $field_widget['#maxlength'] = 60;
    $field_widget['#autocomplete_path'] = 'entity_backend_search/autocomplete/user';
    $field_widget['#description'] = t('Enter username. Separate multiple usernames with commas.');
  }

  if ($context['entity_type'] != 'user') {
    return;
  }

  // User entity specific alterations.
  switch ($context['field_name']) {
    case 'status':
      $field_widget['#options'] = array(
        0 => t('blocked'),
        1 => t('active'),
      );
      break;

  }
}

/**
 * Implements hook_entity_backend_search_autocomplete_title_field().
 */
function user_entity_backend_search_autocomplete_title_field($entity_type) {
  if ($entity_type == 'user') {
    return 'name';
  }
}

// -----------------------------------------------------------------------------
// Bulk operations.
/**
 * Implements hook_entity_backend_search_bulk_operations_list().
 *
 * Utilizes functionality from the user admin page.
 */
function user_entity_backend_search_bulk_operations_list($entity_type) {
  if ($entity_type != 'user' || !user_access('administer users')) {
    return;
  }

  module_load_include('inc', 'user', 'user.admin');
  $options = array();
  foreach (module_invoke_all('user_operations') as $operation => $array) {
    // For now we don't support role manipulation. Cancel is handled a bit
    // different as well, skipping confirm form.
    if (is_array($array['label']) || $operation == 'cancel') {
      continue;
    }

    $options[$operation] = $array['label'];
  }

  // Get cancel operations as well.
  module_load_include('inc', 'user', 'user.pages');
  foreach (user_cancel_methods() as $method => $array) {
    $options[$array['#return_value']] = strip_tags($array['#title']);
  }

  return $options;
}

/**
 * Implements hook_entity_backend_search_bulk_operations_action().
 *
 * Utilizes functionality from the user admin page.
 */
function user_entity_backend_search_bulk_operations_action($entity_type, $op, $entity_ids) {
  if ($entity_type != 'user' || !user_access('administer users')) {
    return;
  }

  module_load_include('inc', 'user', 'user.admin');

  $operations = module_invoke_all('user_operations');
  $operation = isset($operations[$op]) ? $operations[$op] : array();

  if (isset($operation['callback']) && ($function = $operation['callback'])) {
    // Add in callback arguments if present.
    if (isset($operation['callback arguments'])) {
      $args = array_merge(array($entity_ids), $operation['callback arguments']);
    }
    else {
      $args = array($entity_ids);
    }
    call_user_func_array($function, $args);
    drupal_set_message(t('The update has been performed.'));
  }
  elseif (strpos($op, 'user_cancel_') === 0) {
    // User cancel is handled a bit differently than Drupal Core handles them.
    // Skip the confirm form. Run through each uid and cancel the user.
    global $user;

    foreach ($entity_ids as $uid) {
      if ($uid <= 1) {
        continue;
      }
      // Prevent user administrators from deleting themselves without
      // confirmation.
      if ($uid == $user->uid) {
        drupal_set_message(t('You were prevented from cancel your own account.'), 'warning');
        continue;
      }
      user_cancel(array(), $uid, $op);
    }
  }
  else {
    $label = isset($operation['label']) ? $operation['label'] : $op;
    drupal_set_message(t('Unknown action: %operation', array('%operation' => $label)), 'error');
  }
}

// -----------------------------------------------------------------------------
// Search query.
/**
 * Implements hook_entity_backend_search_query_alter().
 */
function user_entity_backend_search_query_alter(&$query, $entity_type, $search_filters) {
  // Handle author by username.
  // If a match on the pattern [entity_type:entity_id] is found, we can should
  // be able to assume that it is an autocomplete field.
  if (!empty($search_filters['uid']) && is_string($search_filters['uid'])) {
    $author_usernames = drupal_explode_tags($search_filters['uid']);
    $uids = array();
    foreach ($author_usernames as $author_username) {
      $matches = array();
      preg_match('/.+\[user:(\d+)\]$/', $author_username, $matches);
      if (isset($matches[1]) && is_numeric($matches[1])) {
        $uids[$matches[1]] = $matches[1];
      }
    }
    if (!empty($uids)) {
      $query->propertyCondition('uid', $uids, 'IN');
    }
  }

  if (!empty($search_filters['user_role_id'])) {
    // Support single select.
    $user_roles = is_array($search_filters['user_role_id']) ? $search_filters['user_role_id'] : array($search_filters['user_role_id']);
    $query->innerJoin('users_roles', 'ur', 'ur.uid = users.uid');
    $query->condition('ur.rid', $user_roles, 'IN');
  }
}

// -----------------------------------------------------------------------------
// Table formatter.
/**
 * Implements hook_entity_backend_search_format_search_result_table().
 */
function user_entity_backend_search_format_search_result_table($entity_type, $entities, &$table_header, &$table_rows) {

  if ($entity_type != 'user') {
    return;
  }

  $table_header = array(
    'profile_picture' => array(
      'data' => t('Image'),
    ),
    'username' => array(
      'data' => t('Username'),
      'field' => 'u.name',
      'type' => 'property',
      'specifier' => 'name',
    ),
    'status' => array(
      'data' => t('Status'),
      'field' => 'u.status',
      'type' => 'property',
      'specifier' => 'status',
    ),
    'roles' => array('data' => t('Roles')),
    'created' => array(
      'data' => t('Registered'),
      'field' => 'u.created',
      'sort' => 'desc',
      'type' => 'property',
      'specifier' => 'created',
    ),
    'access' => array(
      'data' => t('Last access'),
      'field' => 'u.access',
      'type' => 'property',
      'specifier' => 'access',
    ),
    'operations' => array(
      'data' => t('Operations'),
    ),
  );

  // Do an early return with TRUE telling the Entity Search module, that a table
  // formatter was found.
  if (empty($entities)) {
    return TRUE;
  }

  $destination = drupal_get_destination();

  $status = array(t('blocked'), t('active'));
  $roles = array_map('check_plain', user_roles(TRUE));

  foreach ($entities as $account) {
    $users_roles = array();
    $roles_result = db_query('SELECT rid FROM {users_roles} WHERE uid = :uid', array(':uid' => $account->uid));
    foreach ($roles_result as $user_role) {
      $users_roles[] = $roles[$user_role->rid];
    }
    asort($users_roles);

    $profile_picture = '';
    if (!empty($account->picture)) {
      $profile_picture = theme(
        'image_style',
        array(
          'style_name' => 'thumbnail',
          'path' => $account->picture->uri,
        )
      );
    }

    $table_rows[$account->uid] = array(
      'profile_picture' => $profile_picture,
      'username' => theme('username', array('account' => $account)),
      'status' => $status[$account->status],
      'roles' => theme('item_list', array('items' => $users_roles)),
      'created' => format_date($account->created, 'short') . ' (' . format_interval(REQUEST_TIME - $account->created) . ')',
      'access' => $account->access ? t('@time ago', array('@time' => format_interval(REQUEST_TIME - $account->access))) : t('never'),
      'operations' => array(
        'data' => array(
          '#type' => 'link',
          '#title' => t('edit'),
          '#href' => "user/$account->uid/edit",
          '#options' => array('query' => $destination),
        ),
      ),
    );
  }

  // Return with TRUE telling the Entity Search module, that a table formatter
  // was found.
  return TRUE;
}
