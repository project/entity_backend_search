<?php
/**
 * @file
 * Entity Search integration for the comment module.
 */

/**
 * Implements hook_entity_backend_search_format_search_result_table().
 */
function comment_entity_backend_search_format_search_result_table($entity_type, $entities, &$table_header, &$table_rows) {

  if ($entity_type != 'comment') {
    return;
  }

  $table_header = array(
    'subject' => array(
      'data' => t('Subject'),
      'field' => 'subject',
      'type' => 'property',
      'specifier' => 'subject',
    ),
    'author' => array(
      'data' => t('Author'),
      'field' => 'name',
      'type' => 'property',
      'specifier' => 'name',
    ),
    'nid' => array(
      'data' => t('Posted in'),
      'field' => 'nid',
      'type' => 'property',
      'specifier' => 'nid',
    ),
    'changed' => array(
      'data' => t('Updated'),
      'field' => 'c.changed',
      'sort' => 'desc',
      'type' => 'property',
      'specifier' => 'changed',
    ),
    'created' => array(
      'data' => t('Created'),
      'field' => 'c.created',
      'sort' => 'desc',
      'type' => 'property',
      'specifier' => 'created',
    ),
    'status' => array(
      'data' => t('Status'),
      'field' => 'c.status',
      'type' => 'property',
      'specifier' => 'status',
    ),
    'operations' => array(
      'data' => t('Operations'),
    ),
  );

  // Do an early return with TRUE telling the Entity Search module, that a table
  // formatter was found.
  if (empty($entities)) {
    return TRUE;
  }

  // Build a table listing the appropriate comments.
  $options = array();
  $destination = drupal_get_destination();

  $nids_posted_in = array();
  foreach ($entities as $comment) {
    $nids_posted_in[$comment->nid] = $comment->nid;
  }

  // Get titles of nodes the comments are posted at.
  $nids_posted_in_titles = array();
  $node_title_result = db_select('node', 'n')
    ->fields('n', array('nid', 'title'))
    ->condition('nid', $nids_posted_in, 'IN')
    ->execute();
  foreach ($node_title_result as $row) {
    $nids_posted_in_titles[$row->nid] = $row->title;
  }

  foreach ($entities as $comment) {
    // Remove the first node title from the node_titles array and attach to
    // the comment.
    $comment->node_title = 'meh';
    $table_rows[$comment->cid] = array(
      'subject' => array(
        'data' => array(
          '#type' => 'link',
          '#title' => $comment->subject,
          '#href' => 'comment/' . $comment->cid,
          '#options' => array(
            'attributes' => array(
              'title' => truncate_utf8($comment->comment_body[LANGUAGE_NONE][0]['value'], 128),
            ),
            'fragment' => 'comment-' . $comment->cid,
          ),
        ),
      ),
      'author' => theme('username', array('account' => $comment)),
      'posted_in' => array(
        'data' => array(
          '#type' => 'link',
          '#title' => isset($nids_posted_in_titles[$comment->nid]) ? $nids_posted_in_titles[$comment->nid] : t('Unknown title'),
          '#href' => 'node/' . $comment->nid,
        ),
      ),
      'changed' => format_date($comment->changed, 'short'),
      'created' => format_date($comment->changed, 'short'),
      'status' => $comment->status ? t('published') : t('not published'),
      'operations' => array(
        'data' => array(
          '#type' => 'link',
          '#title' => t('edit'),
          '#href' => 'comment/' . $comment->cid . '/edit',
          '#options' => array('query' => $destination),
        ),
      ),
    );
  }

  // Return with TRUE telling the Entity Search module, that a table formatter
  // was found.
  return TRUE;
}
