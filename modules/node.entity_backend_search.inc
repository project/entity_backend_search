<?php
/**
 * @file
 * Entity Search integration for the node module.
 */

// -----------------------------------------------------------------------------
// Filter fields.
/**
 * Implements hook_entity_backend_search_field_widget_alter().
 */
function node_entity_backend_search_field_widget_alter(&$field_widget, &$context) {
  // General alterations.
  if ($context['field_name'] == 'nid' && $context['field_settings']['filter_widget_types'] == 'textfield_autocomplete') {
    // Add autocomplete for node using the entity backend search autocomplete.
    $field_widget['#title'] = t('Node');
    $field_widget['#maxlength'] = 60;
    $field_widget['#autocomplete_path'] = 'entity_backend_search/autocomplete/node';
    $field_widget['#description'] = t('Enter one title to search for.');
  }

  if ($context['entity_type'] != 'node') {
    return;
  }

  // Node entity specific alterations.
  switch ($context['field_name']) {
    case 'status':
      $field_widget['#options'] = array(
        NODE_PUBLISHED => t('Published'),
        NODE_NOT_PUBLISHED => t('Unpublished'),
      );
      break;

  }
}

/**
 * Implements hook_entity_backend_search_autocomplete_title_field().
 */
function node_entity_backend_search_autocomplete_title_field($entity_type) {
  if ($entity_type == 'node') {
    return array(
      'field name' => 'title',
      // Since titles for nodes allows for commas, we can't support multiple
      // autocompleted nodes.
      'multiple tags' => FALSE,
    );
  }
}

// -----------------------------------------------------------------------------
// Bulk operations.
/**
 * Implements hook_entity_backend_search_bulk_operations_list().
 *
 * Utilizes functionality from the node admin page.
 */
function node_entity_backend_search_bulk_operations_list($entity_type) {
  if ($entity_type != 'node' || !user_access('administer nodes')) {
    return;
  }

  module_load_include('inc', 'node', 'node.admin');
  $options = array();
  foreach (module_invoke_all('node_operations') as $operation => $array) {
    $options[$operation] = $array['label'];
  }

  return $options;
}

/**
 * Implements hook_entity_backend_search_bulk_operations_action().
 *
 * Utilizes functionality from the node admin page.
 */
function node_entity_backend_search_bulk_operations_action($entity_type, $op, $entity_ids) {
  if ($entity_type != 'node' || !user_access('administer nodes')) {
    return;
  }

  module_load_include('inc', 'node', 'node.admin');

  $operations = module_invoke_all('node_operations');
  $operation = $operations[$op];

  if ($function = $operation['callback']) {
    // Add in callback arguments if present.
    if (isset($operation['callback arguments'])) {
      $args = array_merge(array($entity_ids), $operation['callback arguments']);
    }
    else {
      $args = array($entity_ids);
    }
    call_user_func_array($function, $args);

    drupal_set_message(t('The update has been performed.'));

    cache_clear_all();
  }
  elseif ($op == 'delete') {
    // Skip the confirm form.
    node_delete_multiple($entity_ids);

    drupal_set_message(t('The update has been performed.'));
  }
  else {
    $label = isset($operation['label']) ? $operation['label'] : $op;
    drupal_set_message(t('Unknown action: %operation', array('%operation' => $label)), 'error');
  }
}

// -----------------------------------------------------------------------------
// Search query.
/**
 * Implements hook_entity_backend_search_query_alter().
 */
function node_entity_backend_search_query_alter(&$query, $entity_type, $search_filters) {
  // Handle autocompleted nodes.
  // If a match on the pattern [entity_type:entity_id] is found, we can should
  // be able to assume that it is an autocomplete field.
  if (!empty($search_filters['nid']) && is_string($search_filters['nid'])) {
    $nid = FALSE;
    $matches = array();
    preg_match('/.+\[node:(\d+)\]$/', $search_filters['nid'], $matches);
    if (isset($matches[1]) && is_numeric($matches[1])) {
      $nid = $matches[1];
    }
    if (!empty($nid)) {
      $query->propertyCondition('nid', $nid);
    }
  }
}

// -----------------------------------------------------------------------------
// Table formatter.
/**
 * Implements hook_entity_backend_search_format_search_result_table().
 */
function node_entity_backend_search_format_search_result_table($entity_type, $entities, &$table_header, &$table_rows) {

  if ($entity_type != 'node') {
    return;
  }

  // Enable language column if translation module is enabled or if we have any
  // node with language.
  $multilanguage = (module_exists('translation') || db_query_range("SELECT 1 FROM {node} WHERE language <> :language", 0, 1, array(':language' => LANGUAGE_NONE))->fetchField());

  // Build the sortable table header.
  $table_header = array(
    'title' => array(
      'data' => t('Title'),
      'field' => 'n.title',
      'type' => 'property',
      'specifier' => 'title',
    ),
    'type' => array(
      'data' => t('Type'),
      'field' => 'n.type',
      'type' => 'property',
      'specifier' => 'type',
    ),
    'author' => t('Author'),
    'status' => array(
      'data' => t('Status'),
      'field' => 'n.status',
      'type' => 'property',
      'specifier' => 'status',
    ),
    'changed' => array(
      'data' => t('Updated'),
      'field' => 'n.changed',
      'sort' => 'desc',
      'type' => 'property',
      'specifier' => 'changed',
    ),
    'created' => array(
      'data' => t('Created'),
      'field' => 'n.created',
      'type' => 'property',
      'specifier' => 'created',
    ),
  );
  if ($multilanguage) {
    $table_header['language'] = array(
      'data' => t('Language'),
      'field' => 'n.language',
      'type' => 'property',
      'specifier' => 'language',
    );
  }
  $table_header['operations'] = array('data' => t('Operations'));

  // Do an early return with TRUE telling the Entity Search module, that a table
  // formatter was found.
  if (empty($entities)) {
    return TRUE;
  }

  $destination = drupal_get_destination();
  $languages = language_list();
  foreach ($entities as $node) {
    $langcode = entity_language('node', $node);
    $l_options = $langcode != LANGUAGE_NONE && isset($languages[$langcode]) ? array('language' => $languages[$langcode]) : array();
    $table_rows[$node->nid] = array(
      'title' => array(
        'data' => array(
          '#type' => 'link',
          '#title' => $node->title,
          '#href' => 'node/' . $node->nid,
          '#options' => $l_options,
          '#suffix' => ' ' . theme('mark', array('type' => node_mark($node->nid, $node->changed))),
        ),
      ),
      'type' => check_plain(node_type_get_name($node)),
      'author' => theme('username', array('account' => $node)),
      'status' => $node->status ? t('published') : t('not published'),
      'changed' => format_date($node->changed, 'short'),
      'created' => format_date($node->created, 'short'),
    );
    if ($multilanguage) {
      if ($langcode == LANGUAGE_NONE || isset($languages[$langcode])) {
        $table_rows[$node->nid]['language'] = $langcode == LANGUAGE_NONE ? t('Language neutral') : $languages[$langcode]->native;
      }
      else {
        $table_rows[$node->nid]['language'] = t('Undefined language (@langcode)', array('@langcode' => $langcode));
      }
    }
    // Build a list of all the accessible operations for the current node.
    $operations = array();
    if (node_access('update', $node)) {
      $operations['edit'] = array(
        'title' => t('edit'),
        'href' => 'node/' . $node->nid . '/edit',
        'query' => $destination,
      );
    }
    if (node_access('delete', $node)) {
      $operations['delete'] = array(
        'title' => t('delete'),
        'href' => 'node/' . $node->nid . '/delete',
        'query' => $destination,
      );
    }
    $table_rows[$node->nid]['operations'] = array();
    if (count($operations) > 1) {
      // Render an unordered list of operations links.
      $table_rows[$node->nid]['operations'] = array(
        'data' => array(
          '#theme' => 'links__node_operations',
          '#links' => $operations,
          '#attributes' => array('class' => array('links', 'inline')),
        ),
      );
    }
    elseif (!empty($operations)) {
      // Render the first and only operation as a link.
      $link = reset($operations);
      $table_rows[$node->nid]['operations'] = array(
        'data' => array(
          '#type' => 'link',
          '#title' => $link['title'],
          '#href' => $link['href'],
          '#options' => array('query' => $link['query']),
        ),
      );
    }

  }

  // Return with TRUE telling the Entity Search module, that a table formatter
  // was found.
  return TRUE;
}
