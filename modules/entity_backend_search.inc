<?php

/**
 * @file
 * Some general implementations which helps the wheels turn of this module.
 */

// -----------------------------------------------------------------------------
// Search Entity hooks.
/**
 * Implements hook_entity_backend_search_filter_types().
 */
function entity_backend_search_entity_backend_search_filter_types($entity_type, $field_name) {
  static $entities_base_table_schemas = array();
  $filters = array();

  // Implement some of the basic field types.
  if ($field_info_field = field_info_field($field_name)) {

    // Implement some of the basic field types.
    switch ($field_info_field['module']) {
      case 'text':
        $filters['textfield_equal'] = t('Textfield (Equal to "input")');
        $filters['textfield_contains'] = t('Textfield (Contains "%input%"")');
        break;

      case 'list':
        $filters['select_multiple'] = t('Select multiple');
        break;

      case 'date':
        $filters['date_from_to'] = t('Date from to (simple input field)');
        if (module_exists('date_popup')) {
          $filters['date_from_to_date_popup'] = t('Date from to (using Date Popup)');
        }
        break;

      default:
        $filters['textfield_equal'] = t('Textfield (Equal to "input")');
        break;
    }

    return $filters;
  }

  $entity_base_table = entity_backend_search_entity_base_table($entity_type);

  // A few assumptions based on the entity properties to help push the available
  // filter options along.
  if ($entity_base_table && isset($entity_base_table['fields']) && isset($entity_base_table['fields'][$field_name])) {
    switch ($entity_base_table['fields'][$field_name]['type']) {
      case 'char':
      case 'varchar':
      case 'text':
      case 'blob':
        $filters['textfield_equal'] = t('Textfield (Equal to "input")');
        $filters['textfield_contains'] = t('Textfield (Contains "%input%"")');
        break;

      case 'int':
      case 'float':
      case 'numeric':
        $filters['textfield_equal'] = t('Textfield (Equal to "input")');
        break;

      case 'date':
      case 'datetime':
        $filters['date_from_to'] = t('Date from to (simple input field)');
        if (module_exists('date_popup')) {
          $filters['date_from_to_date_popup'] = t('Date from to (using Date Popup)');
        }
        break;

    }
  }

  // A few assumptions based on the field name to help push the available filter
  // options along.
  switch ($field_name) {
    case 'created':
    case 'changed':
    case 'access':
    case 'login':
    case 'timestamp':

      $filters['date_from_to'] = t('Date from to (simple input field)');
      if (module_exists('date_popup')) {
        $filters['date_from_to_date_popup'] = t('Date from to (using Date Popup)');
      }
      break;

    case 'status':
    case 'promote':
    case 'sticky':
    case 'role':
    case 'language':
    case 'timezone':
      $filters['select_single'] = t('Select single');
      $filters['select_multiple'] = t('Select multiple');

      break;

    case 'uid':
    case 'nid':
    case 'tid':
      $filters['textfield_equal'] = t('Textfield (Equal to "input")');
      $filters['textfield_autocomplete'] = t('Textfield Autocomplete');
      break;
  }

  return $filters;
}

/**
 * Implements hook_entity_backend_search_field_widget().
 */
function entity_backend_search_entity_backend_search_field_widget($entity_type, $field_name, $field_settings, $default_values) {
  // First get label of field.
  $field_label = ucfirst(str_replace(array('-', '_'), ' ', $field_name));

  $field_widget = FALSE;

  switch ($field_settings['filter_widget_types']) {
    case 'textfield_equal':
      $field_widget = array(
        '#type' => 'textfield',
        '#title' => check_plain($field_label),
        '#description' => t('@label matches entered string.', array('@label' => $field_label)),
        '#default_value' => isset($default_values[$field_name]) ? $default_values[$field_name] : NULL,
      );
      break;

    case 'textfield_contains':
      $field_widget = array(
        '#type' => 'textfield',
        '#title' => check_plain($field_label),
        '#description' => t('@label contains entered string.', array('@label' => $field_label)),
        '#default_value' => isset($default_values[$field_name]) ? $default_values[$field_name] : NULL,
      );
      break;

    case 'select_single':
      $field_widget = _entity_backend_search_field_widget_get_additional_info($entity_type, $field_name, $field_settings, $default_values);
      $field_widget += array(
        '#type' => 'select',
        '#title' => check_plain($field_label),
        '#description' => t('@label equals selected item.', array('@label' => $field_label)),
        '#options' => array(),
        '#default_value' => isset($default_values[$field_name]) ? $default_values[$field_name] : array(),
      );
      break;

    case 'select_multiple':
      $field_widget = _entity_backend_search_field_widget_get_additional_info($entity_type, $field_name, $field_settings, $default_values);
      $field_widget += array(
        '#type' => 'select',
        '#multiple' => TRUE,
        '#title' => check_plain($field_label),
        '#description' => t('@label equals one of the selected items.', array('@label' => $field_label)),
        '#options' => array(),
        '#default_value' => isset($default_values[$field_name]) ? $default_values[$field_name] : array(),
      );
      break;

    case 'textfield_autocomplete':
      $field_widget = _entity_backend_search_field_widget_get_additional_info($entity_type, $field_name, $field_settings, $default_values);
      $field_widget += array(
        '#type' => 'textfield',
        '#title' => check_plain($field_label),
        '#description' => t('@label contains entered string.', array('@label' => $field_label)),
        '#default_value' => isset($default_values[$field_name]) ? $default_values[$field_name] : NULL,
      );
      break;

    case 'date_from_to_date_popup':
    case 'date_from_to':
      // First figure out if we use the date_popup widget. Then set the input
      // format, which is only actually used when we are displaying the
      // date_popup widget. #date_year_range is also only used for the
      // date_popup widget.
      $date_popup = $field_settings['filter_widget_types'] == 'date_from_to_date_popup' && module_exists('date_popup');

      $date_format = NULL;
      if ($date_popup) {
        $date_format = date_limit_format(
          variable_get('date_format_short', 'm/d/Y - H:i'),
          array('year', 'month', 'day')
        );
      }

      // @todo: Figure out the tree depth issue!
      $field_widget = array(
        '#type' => 'fieldset',
        '#title' => t('Date @label before/after', array('@label' => $field_name)),
        '#tree' => TRUE,
        '#parents' => array(
          $field_name,
        ),
      );
      $field_widget['before'] = array(
        '#type' => $date_popup ? 'date_popup' : 'textfield',
        '#title' => t('@label before:', array('@label' => $field_label)),
        '#description' => $date_popup ? NULL : t('The date format is YYYY-MM-DD'),
        '#date_year_range' => '-10:+3',
        '#date_format' => $date_format,
        '#default_value' => empty($default_values[$field_name]['before']) ? NULL : $default_values[$field_name]['before'],
      );
      $field_widget['after'] = array(
        '#type' => $date_popup ? 'date_popup' : 'textfield',
        '#title' => t('@label after', array('@label' => $field_label)),
        '#description' => $date_popup ? NULL : t('The date format is YYYY-MM-DD'),
        '#date_year_range' => '-10:+3',
        '#date_format' => $date_format,
        '#default_value' => empty($default_values[$field_name]['after']) ? NULL : $default_values[$field_name]['after'],
      );

      break;
  }

  return $field_widget;
}

/**
 * Set defaults for the filter widgets in the search form.
 *
 * @param string $entity_type
 *   The entity type we are working with.
 * @param string $field_name
 *   The name of the field being processed.
 * @param array $field_settings
 *   An array containing the settings set on the configuration page for this
 *   module.
 * @param array $default_values
 *   An array containing default values, if any.
 *
 * @return array
 *   A set of defaults for the filter widget form element.
 */
function _entity_backend_search_field_widget_get_additional_info($entity_type, $field_name, $field_settings, $default_values = array()) {
  $field_widget = array();

  // General assumptions.
  switch ($field_name) {
    case 'language':
      $options = array();
      foreach (language_list() as $lang) {
        $options[$lang->language] = $lang->name;
      }
      $field_widget['#options'] = $options;
      break;

    case 'promote':
      $options = array(
        NODE_PROMOTED => t('Promoted'),
        NODE_NOT_PROMOTED => t('Not promoted'),
      );
      $field_widget['#options'] = $options;
      break;

    case 'sticky':
      $options = array(
        NODE_STICKY => t('Sticky'),
        NODE_NOT_STICKY => t('Not sticky'),
      );
      $field_widget['#options'] = $options;
      break;

    case 'status':
      $options = array(
        0 => t('No'),
        1 => t('Yes'),
      );
      $field_widget['#options'] = $options;
      break;
  }

  // Implement some of the basic field types.
  if ($field_info_field = field_info_field($field_name)) {
    switch ($field_info_field['module']) {
      case 'list':
        $options = array();
        // Set the default values if none has been inputted in the field. This
        // is standard behaviour of this field type.
        foreach ($field_info_field['settings']['allowed_values'] as $value => $option) {
          $options[$value] = $option == '' ? $value : $option;
        }
        if (count($options) == 2 && isset($options[0]) && $options[0] == 0 && isset($options[1]) && $options[1] == 1) {
          $options = array(
            1 => t('On'),
            0 => t('Off'),
          );
        }
        $field_widget['#options'] = $options;
        break;
    }
  }

  return $field_widget;
}

/**
 * Implements hook_entity_backend_search_query_alter().
 *
 * Handle the basic search query object build. This is pretty much the core
 * functionality of the module, but we keep it in a hook to let modules weighted
 * before this implementation of the hook is onvoked to let them unset or alter
 * filters values as needed.
 */
function entity_backend_search_entity_backend_search_query_alter(&$query, $entity_type, $search_filters) {
  $settings = entity_backend_search_get_search_settings($entity_type);

  $entity_base_table = entity_backend_search_entity_base_table($entity_type);

  foreach ($search_filters as $field_name => $filter_values) {
    $filter_widget_type = FALSE;
    $field_type = FALSE;
    if (isset($settings['fields'][$field_name]) && !empty($settings['fields'][$field_name]['filter_widget_types'])) {
      $filter_widget_type = $settings['fields'][$field_name]['filter_widget_types'];
    }
    else {
      continue;
    }

    $field = NULL;
    // Figure out if it is a field or a property.
    if ($field = field_info_field($field_name)) {
      $field_type = 'field';
    }
    elseif ($entity_base_table && isset($entity_base_table['fields']) && isset($entity_base_table['fields'][$field_name])) {
      $field = $entity_base_table['fields'][$field_name];
      $field_type = 'property';
    }

    if (!$field_type) {
      continue;
    }

    // Make sure that we actually needs to add filters.
    if ($filter_values == '') {
      continue;
    }
    if (is_array($filter_values) && count($filter_values) == 0) {
      continue;
    }

    switch ($filter_widget_type) {
      case 'select_single':
      case 'textfield_equal':
        if ($field_type == 'property') {
          $query->propertyCondition($field_name, $filter_values, '=');
        }
        elseif ($field_type == 'field' && isset($field['columns']) && is_array($field['columns'])) {
          if ($column = key($field['columns'])) {
            $query->fieldCondition($field_name, $column, $filter_values, '=');
          }
        }
        break;

      case 'textfield_starts_with':
        if ($field_type == 'property') {
          $query->propertyCondition($field_name, $filter_values, 'STARTS_WITH');
        }
        elseif ($field_type == 'field') {
          $query->fieldCondition($field_name, 'value', $filter_values, 'STARTS_WITH');
        }
        break;

      case 'textfield_contains':
        if ($field_type == 'property') {
          $query->propertyCondition($field_name, $filter_values, 'CONTAINS');
        }
        elseif ($field_type == 'field') {
          $query->fieldCondition($field_name, 'value', $filter_values, 'CONTAINS');
        }
        break;

      case 'select_multiple':
        if ($field_type == 'property') {
          $query->propertyCondition($field_name, $filter_values, 'IN');
        }
        elseif ($field_type == 'field' && isset($field['columns']) && is_array($field['columns'])) {
          if ($column = key($field['columns'])) {
            $query->fieldCondition($field_name, $column, $filter_values, 'IN');
          }
        }

        break;

      case 'date_from_to':
      case 'date_from_to_date_popup':
        // Map input date filter values to the database format.
        foreach ($filter_values as $key => $value) {
          if (empty($value)) {
            continue;
          }
          if ($field_type == 'field' && $field['module'] == 'date') {
            $timezone_db = date_get_timezone_db($field['settings']['tz_handling']);
            $db_format = date_type_format($field['type']);

            $date = new DateObject($value, $timezone_db);
            $filter_values[$key] = $date->format($db_format);
          }
          else {
            // The "datetime" seems to be unsupported by Drupal 7, but still
            // listed at http://drupal.org/node/159605. Assume int.
            $filter_values[$key] = strtotime($value);
          }
        }

        // Add before and/or after filters to query object.
        if (!empty($filter_values['before']) && !empty($filter_values['after'])) {
          $created_between = array(
            $filter_values['after'],
            $filter_values['before'],
          );

          if ($field_type == 'property') {
            $query->propertyCondition($field_name, $created_between, 'BETWEEN');
          }
          elseif ($field_type == 'field') {
            $query->fieldCondition($field_name, 'value', $created_between, 'BETWEEN');
          }
        }
        elseif (!empty($filter_values['before'])) {
          if ($field_type == 'property') {
            $query->propertyCondition($field_name, $filter_values['before'], '<=');
          }
          elseif ($field_type == 'field') {
            $query->fieldCondition($field_name, 'value', $filter_values['before'], '<=');
          }
        }
        elseif (!empty($filter_values['after'])) {
          if ($field_type == 'property') {
            $query->propertyCondition($field_name, $filter_values['after'], '>=');
          }
          elseif ($field_type == 'field') {
            $query->fieldCondition($field_name, 'value', $filter_values['after'], '>=');
          }
        }

        break;
    }
  }
}
