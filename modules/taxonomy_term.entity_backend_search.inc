<?php
/**
 * @file
 * Entity Search integration for the taxonomy module.
 */

/**
 * Implements hook_entity_backend_search_filter_types().
 */
function taxonomy_entity_backend_search_filter_types($entity_type, $field_name) {
  $filters = array();

  if ($entity_type == 'taxonomy_term' && $field_name == 'vid') {
    $filters['select_single'] = t('Select single');
    $filters['select_multiple'] = t('Select multiple');
    return $filters;
  }

  if (($field_info_field = field_info_field($field_name)) && $field_info_field['module'] == 'taxonomy') {
    $filters['select_multiple'] = t('Select multiple');
    $filters['textfield_autocomplete'] = t('Textfield Autocomplete');
    return $filters;
  }
}

/**
 * Implements hook_entity_backend_search_field_widget_alter().
 */
function taxonomy_entity_backend_search_field_widget_alter(&$field_widget, $context) {
  if ($context['entity_type'] == 'taxonomy_term' && $context['field_name'] == 'vid') {
    // Add vocabulary options to term filtering.
    $field_widget['#options'] = array();
    foreach (taxonomy_get_vocabularies() as $vocabulary) {
      $field_widget['#options'][$vocabulary->vid] = $vocabulary->name;
    }
    return;
  }

  if (($field = field_info_field($context['field_name'])) && $field['module'] == 'taxonomy') {
    if ($context['field_settings']['filter_widget_types'] == 'select_multiple') {
      $field_widget['#options'] = taxonomy_allowed_values($field);
    }
    elseif ($context['field_settings']['filter_widget_types'] == 'textfield_autocomplete') {
      module_load_include('inc', 'taxonomy', 'taxonomy.pages');
      $field_widget['#autocomplete_path'] = 'taxonomy/autocomplete/' . $context['field_name'];
    }
  }
}

/**
 * Implements hook_entity_backend_search_query_alter().
 */
function taxonomy_entity_backend_search_query_alter(&$query, $entity_type, $search_filters) {
  $settings = entity_backend_search_get_search_settings($entity_type);

  // Handle taxonomy autocomplete for taxonomy fields.
  // Select multiple is being supported by the stanard implementation in the
  // Entity Search default handling of the filters.
  foreach ($search_filters as $field_name => $filter_values) {
    if (empty($filter_values)) {
      continue;
    }

    if (isset($settings['fields'][$field_name]) && $settings['fields'][$field_name]['filter_widget_types'] == 'textfield_autocomplete') {
      if (($field = field_info_field($field_name)) && $field['module'] == 'taxonomy') {

        // Make sure that we have enough info for our fieldCondition.
        if (isset($field['columns']) && is_array($field['columns']) && ($column = key($field['columns']))) {
          // Get term ids from the typed tags and add the condition to the
          // query.
          $tags_typed = drupal_explode_tags($filter_values);

          // Part of the criteria for the query come from the field's own
          // settings.
          $vids = array();
          $vocabularies = taxonomy_vocabulary_get_names();
          foreach ($field['settings']['allowed_values'] as $tree) {
            $vids[] = $vocabularies[$tree['vocabulary']]->vid;
          }
          $tids = db_select('taxonomy_term_data', 't')
            ->fields('t', array('tid'))
            ->condition('t.vid', $vids)
            ->condition('t.name', $tags_typed, 'IN')
            ->execute()
            ->fetchAll(PDO::FETCH_COLUMN, 'tid');

          if (!empty($tids)) {
            $query->fieldCondition($field_name, $column, $tids, 'IN');
          }
          else {
            drupal_set_message(t('None of the given taxonomy terms where found. Terms igored in filtering: %terms', array('%terms' => implode(', ', $tags_typed))), 'warning');
          }
        }
      }
    }
  }
}

/**
 * Implements hook_entity_backend_search_format_search_result_table().
 */
function taxonomy_entity_backend_search_format_search_result_table($entity_type, $entities, &$table_header, &$table_rows) {

  if ($entity_type != 'taxonomy_term') {
    return;
  }

  // Build the sortable table header.
  $table_header = array(
    'name' => array(
      'data' => t('Name'),
      'field' => 't.name',
      'type' => 'property',
      'specifier' => 'name',
    ),
    'vid' => array(
      'data' => t('Vocabulary'),
      'field' => 't.vid',
      'type' => 'property',
      'specifier' => 'vid',
    ),
    'description' => t('Description'),
    'language' => array(
      'data' => t('Language'),
    ),
    'operations' => array(
      'data' => t('Operations'),
    ),
  );

  // Do an early return with TRUE telling the Entity Search module, that a table
  // formatter was found.
  if (empty($entities)) {
    return TRUE;
  }

  $destination = drupal_get_destination();
  $languages = language_list();
  $vocabularies = taxonomy_get_vocabularies();
  foreach ($entities as $term) {
    $langcode = entity_language('taxonomy_term', $term);
    $term_language = ($langcode == LANGUAGE_NONE || !isset($languages[$langcode])) ? t('Language neutral') : $languages[$langcode]->native;

    $table_rows[$term->tid] = array(
      'name' => array(
        'data' => array(
          '#type' => 'link',
          '#title' => check_plain($term->name),
          '#href' => 'taxonomy/term/' . $term->tid,
        ),
      ),
      'vid' => isset($vocabularies[$term->vid]) ? $vocabularies[$term->vid]->name : t('Unknown'),
      'description' => truncate_utf8(check_plain($term->description), 128),
      'language' => $term_language,
      'operations' => array(
        'data' => array(
          '#type' => 'link',
          '#title' => t('edit'),
          '#href' => 'taxonomy/term/' . $term->tid . '/edit',
          '#options' => array(
            'query' => drupal_get_destination(),
          ),
        ),
      ),
    );
  }

  // Return with TRUE telling the Entity Search module, that a table formatter
  // was found.
  return TRUE;
}
