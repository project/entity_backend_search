<?php
/**
 * @file
 * Entity Search integration for the file module.
 */

// -----------------------------------------------------------------------------
// Table formatter.
/**
 * Implements hook_entity_backend_search_table_field_formaters_list_alter().
 */
function file_entity_backend_search_table_field_formaters_list_alter(&$formatters, $context) {
  if ($context['entity_type'] != 'file') {
    return;
  }

  switch ($context['field_name']) {
    case 'fid':
      if (module_exists('file_entity')) {
        $formatters['file_edit_link'] = t('File edit');
      }
      break;

    case 'uri':
      if (module_exists('image')) {
        $formatters['file_image_thumbnail'] = t('Image thumbnail or filename');
      }
      break;

    case 'filemime':
    case 'filename':
      $formatters['file_link_icon'] = t('File link with mime type icon');
      break;
  }
}

/**
 * Implements hook_entity_backend_search_table_field_formaters_render_alter().
 */
function file_entity_backend_search_table_field_formaters_render_alter(&$field_value, $context) {
  switch ($context['formatter']) {
    case 'file_edit_link':
      $field_value = l(t('Edit'), 'file/' . $field_value . '/edit');
      break;

    case 'file_image_thumbnail':
      // file_validate_is_image() returns empty array if it is an image... So if
      // it is an emty array, it validated.
      $is_not_image = file_validate_is_image($context['entity']);
      if (empty($is_not_image)) {
        $variables = array(
          'style_name' => 'thumbnail',
          'path' => $context['entity']->uri,
        );
        $field_value = theme('image_style', $variables);
      }
      break;

    case 'file_link_icon':
      $field_value = theme('file_link', array('file' => $context['entity']));
      break;
  }
}
