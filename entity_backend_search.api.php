<?php

/**
 * @file
 * Hooks provided by the Backend Editorial Search API.
 */

// -----------------------------------------------------------------------------
// Filter fields.
/**
 * Add custom filter types to backend configuration.
 *
 * These filter names are used both when building the administration page as
 * well as when the search page is built.
 *
 * @param string $entity_type
 *   The name of the entity type: node, user etc.
 * @param string $field_name
 *   The name of the field to filter. This might be an entity field or property.
 *
 * @return array
 *   Returns an array of possible filter types.
 *   filter_machine_name => description
 *
 * @see entity_backend_search_settings_overview()
 * @see hook_entity_backend_search_field_widget_alter()
 */
function hook_entity_backend_search_filter_types($entity_type, $field_name) {
  $filters = array();

  switch ($field_name) {
    case 'cid':
      // Assume that cid is always comment id. Add option autocompletion.
      $filters['textfield_autocomplete'] = t('Textfield Autocomplete');
      break;
  }

  return $filters;
}

/**
 * Add search filter form elements to search page.
 *
 * @param string $entity_type
 *   The name of the entity type: node, user etc.
 * @param string $field_name
 *   The name of the field to filter. This might be an entity field or property.
 * @param array $field_settings
 *   Array of field settings from the configuration page.
 * @param array $default_values
 *   Previously submitted search values from the search form, indexed by the
 *   field name.
 *
 * @return mixed
 *   Either an array descriping a form element of something with equals FALSE.
 *
 * @see entity_backend_search_filters_form()
 */
function hook_entity_backend_search_field_widget($entity_type, $field_name, $field_settings, $default_values) {
  // Add exact subject search form element to filter form.
  if ($entity_type == 'comment' && $field_name == 'subject' && $field_settings['filter_widget_types'] == 'textfield_equal') {
    $field_widget = array(
      '#type' => 'textfield',
      '#title' => t('Enter exact subject of the comment to search for'),
      '#default_value' => isset($default_values['subject']) ? $default_values['subject'] : NULL,
    );

    return $field_widget;
  }
}

/**
 * Alter filter widgets in search form on the search page.
 *
 * Alter the filter form elements displayed in the search filter form. This is
 * called after the search field has been constructed.
 *
 * @param array $field_widget
 *   Form element displayed in search filter form. Basic Form API.
 * @param array $context
 *   - entity_type: Name of the entity type for the current search page.
 *   - field_name: Name of the field for which the form element has been
 *     constructed.
 *   - field_settings: The configured settings for the field from the
 *     configuration page.
 *
 * @see entity_backend_search_filters_form()
 * @see hook_entity_backend_search_filter_types()
 */
function hook_entity_backend_search_field_widget_alter(&$field_widget, &$context) {
  if ($context['field_name'] == 'cid' && $context['field_settings']['filter_widget_types'] == 'textfield_autocomplete') {
    // Add autocomplete for node using the entity backend search autocomplete.
    $field_widget['#title'] = t('Comment');
    $field_widget['#maxlength'] = 60;
    $field_widget['#autocomplete_path'] = 'entity_backend_search/autocomplete/comment';
    $field_widget['#description'] = t('Enter one subject to search for.');
  }

}

// -----------------------------------------------------------------------------
// Autocomplete functionality.
/**
 * Autocomplete callback.
 *
 * When using the autocomplete functionality supplied by this module, you have
 * the option to define which title field to search on and display as the
 * autocomplete results. By default it will attempt to guess by fetching the
 * label field for the entity type, based on the path.
 *
 * @param string $entity_type
 *   Name of the entity to define title field for.
 *
 * @return mixed
 *   You can either return just the field name as a string or return an array:
 *   - field name: The name of the field to output in autocomplete result.
 *   - multiple tags: A bool indicating whether or not the field should be able
 *     to filter on multiple autocompleted strings. If the field name can
 *     contain commas, it is highly recommended to turn this off. It is on by
 *     default.
 * @see entity_backend_search_autocomplete()
 */
function hook_entity_backend_search_autocomplete_title_field($entity_type) {
  if ($entity_type == 'comment') {
    return array(
      // Field to display in autocomplete results.
      'field name' => 'subject',
      // Since subjects for comments allows for commas, we can't support
      // multiple autocompleted comment subjects.
      'multiple tags' => FALSE,
    );
  }
}

// -----------------------------------------------------------------------------
// Search query.
/**
 * Manipulate the search results query object.
 *
 * When adding custom filters, it is necesary to handle those filters when the
 * query object is being constructed.
 *
 * Handle the basic search query object build..
 *
 * @param object &$query
 *   The query object EntityBackendSearchEntityFieldQuery which extends
 *   EntityFieldQuery adding basic join functionalities. The basic query object
 *   has been constructed and is about to be executed, quering the the results
 *   in the search results table. Add custom filters, sorts etc. as needed to
 *   this object. It is passed by reference.
 * @param string $entity_type
 *   The entity type we are quering on.
 * @param array $search_filters
 *   An array of the submitted search filters.
 *
 * @see entity_backend_search_get_search_result_ids()
 */
function hook_entity_backend_search_query_alter(&$query, $entity_type, $search_filters) {
  // Add autocompleted comment subjects to filtering.
  // If a match on the pattern [entity_type:entity_id] is found, we can should
  // be able to assume that it is an autocomplete field.
  if (!empty($search_filters['cid']) && is_string($search_filters['cid'])) {
    $cid = FALSE;
    $matches = array();
    preg_match('/.+\[comment:(\d+)\]$/', $search_filters['cid'], $matches);
    if (isset($matches[1]) && is_numeric($matches[1])) {
      $cid = $matches[1];
    }
    if (!empty($cid)) {
      $query->propertyCondition('cid', $cid);
    }
  }
}

// -----------------------------------------------------------------------------
// Table formatters.
/**
 * Formatters for values in the search results table.
 *
 * Each column in the configured table of search results can be manipulated
 * before it is being displayed in the search result table. By adding a
 * formatter to the given $formatters array, the alteration can be triggered in
 * hook_entity_backend_search_table_field_formaters_render_alter().
 *
 * @param array $formatters
 *   The formatters contains an array of labelsm indexed by the machine name
 *   of the formatter. This is used when
 *   hook_entity_backend_search_table_field_formaters_list_alter() is called.
 * @param array $context
 *   The context array is indexed by:
 *   - entity_type: The entity type currently being configured.
 *     field_name: The name of the table field in the base table of the entity.
 *   - type: The type of field in the base table. This follows the schema api,
 *     such as int, text etc.
 *
 * @see entity_backend_search_settings_form()
 * @see hook_entity_backend_search_table_field_formaters_render_alter()
 */
function hook_entity_backend_search_table_field_formaters_list_alter(&$formatters, $context) {
  if ($context['field_name'] == 'nid') {
    $formatters['node_title'] = t('Title of the node');
  }
}

/**
 * Manipulate with the values being displayed in the search results table.
 *
 * @param mixed &$field_value
 *   The value about to be displayed in each cell of the search results table.
 *   It is passed by reference, so just replace the value.
 * @param array $context
 *   Index array contains:
 *   - entity_type: The entity type of the entity currently being displayed in
 *     the search result table.
 *   - field_name: The name of the table field in the base table of the entity.
 *   - pattern: If any pattern has been inputted, it can be used to process the
 *     $field_value.
 *   - formatter: The machine name of the formatter. This is the one
 *   - entity
 *
 * @see _entity_backend_search_search_result_table_configured_format()
 * @see hook_entity_backend_search_table_field_formaters_list_alter()
 */
function hook_entity_backend_search_table_field_formaters_render_alter(&$field_value, $context) {
  switch ($context['formater']) {
    case 'node_title':
      if (!is_numeric($field_value)) {
        break;
      }

      $title = db_select('node', 'n')
        ->fields('n', array('title'))
        ->condition('nid', $field_value)
        ->execute()->fetchField();
      if ($title) {
        // Field value was node id. It is now being replaced by the node title
        // link.
        $field_value = l(check_plain($title), 'node/' . $field_value);
      }
      break;
  }
}

/**
 * Render the search results table in code.
 *
 * The search results table configuraion alone might not meet the needs of the
 * client, so it is possible to implement a custom table formatter in code.
 * Check the entity type and manipulate the table header and rows as needed.
 * An array of entities indexed by their ids is supplied in the callback.
 *
 * @param string $entity_type
 *   The entity type being displayed in the results table.
 * @param array $entities
 *   An array of loaded entities, indexed by their entity ids.
 * @param array &$table_header
 *   The table header used in the search results table. It supports sortable
 *   columns.
 * @param array &$table_rows
 *   An empty array of table rows to be filled. Index each row by the entity id,
 *   which is expected through out the rendering of the search results table.
 *
 * @return bool
 *   The table formatter functionality needs to know, that there is a hook
 *   handling the table formatting, so return TRUE when handling the table
 *   formatting for the given entity type.
 *
 * @see entity_backend_search_search_result_table()
 * @see _entity_backend_search_search_result_table_in_code_format()
 */
function hook_entity_backend_search_format_search_result_table($entity_type, $entities, &$table_header, &$table_rows) {

  if ($entity_type != 'node') {
    return;
  }

  // Build the sortable table header.
  $table_header = array(
    'title' => array(
      'data' => t('Title'),
      'field' => 'n.title',
      'type' => 'property',
      'specifier' => 'title',
    ),
    'operations' => array(
      'data' => t('Operations'),
    ),
  );

  foreach ($entities as $entity_id => $entity) {
    $row = array();

    $row['title'] = array(
      'data' => l($entity->title, 'node/' . $entity_id),
    );

    $row['operations'] = array(
      'data' => l(t('Edit'), 'node/' . $entity_id . '/edit'),
    );

    $table_rows[$entity_id] = $row;
  }

  // Return with TRUE telling the Entity Search module, that a table formatter
  // was found.
  return TRUE;
}

/**
 * Alter table results just before rendering the search results table.
 *
 * @param array &$table_header
 *   The table header of the search results table.
 * @param array &$table_rows
 *   The table rows which should be indexed by the entity id for each row.
 * @param array $context
 *   - entity_type: The current entity type.
 *   - entities: An array of loaded entities, indexed by their entity ids.
 */
function hook_entity_backend_search_format_search_result_table_alter(&$table_header, &$table_rows, $context) {
  $table_header['silly column'] = array(
    'data' => t('Silly information'),
  );

  foreach ($table_rows as $id => $row) {
    $table_rows[$id]['silly column'] = array(
      'data' => t('The number is the reminder of $id%42: %number', array('%number' => ($id % 42))),
    );
  }
}

// -----------------------------------------------------------------------------
// Bulk operations.
/**
 * Bulk operations list array.
 *
 * Bulk operations can be added to the search results table. If any operations
 * are found for a table, the options on each table row will be activated
 * automatically.
 * A corresponding action of needed for each operation listed.
 *
 * @param string $entity_type
 *   The entity type being displayed in the results table.
 *
 * @see entity_backend_search_search_result_table()
 * @see hook_entity_backend_search_bulk_operations_action()
 * @see node_entity_backend_search_bulk_operations_action()
 * @see user_entity_backend_search_bulk_operations_action()
 */
function hook_entity_backend_search_bulk_operations_list($entity_type) {
  if ($entity_type != 'file') {
    return;
  }

  $operations = array(
    'delete' => t('Delete files'),
  );

  return $operations;
}

/**
 * Bulk operations actions.
 *
 * Handle triggered operations by matching the entity type and the operation.
 * A list of entity ids is supplied with the call.
 *
 * @param string $entity_type
 *   The entity type being displayed in the results table.
 * @param string $operation
 *   The name of the operation to perform.
 * @param array $entity_ids
 *   An array of entity ids checked in the search results table.
 *
 * @see entity_backend_search_search_result_table()
 * @see hook_entity_backend_search_bulk_operations_list()
 * @see node_entity_backend_search_bulk_operations_list()
 * @see user_entity_backend_search_bulk_operations_list()
 */
function hook_entity_backend_search_bulk_operations_action($entity_type, $operation, $entity_ids) {
  if ($entity_type != 'file') {
    return;
  }

  switch ($operation) {
    case 'delete':
      foreach (file_load_multiple($entity_ids) as $file) {
        file_delete($file);
      }
      break;
  }
}
