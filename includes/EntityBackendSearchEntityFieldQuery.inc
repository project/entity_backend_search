<?php

/**
 * @file
 * Extended EntityFieldQuery to add support for joins and conditions.
 */

class EntityBackendSearchEntityFieldQuery extends EntityFieldQuery {

  /**
   * A list of the joins added to this query.
   *
   * @var array
   */
  protected $joins = array();

  /**
   * A list of the conditions added to this query.
   *
   * @var array
   */
  protected $conditions = array();

  /**
   * Inner Join against another table in the database.
   *
   * When refering to the table, you wish to join up to, give full table name.
   * See user.entity_backend_search.inc.
   *
   * @param string $table
   *   The table against which to join.
   * @param string $alias
   *   The alias for the table. In most cases this should be the first letter
   *   of the table, or the first letter of each "word" in the table.
   * @param string $condition
   *   The condition on which to join this table. If the join requires values,
   *   this clause should use a named placeholder and the value or values to
   *   insert should be passed in the 4th parameter. For the first table joined
   *   on a query, this value is ignored as the first table is taken as the base
   *   table. The token %alias can be used in this string to be replaced with
   *   the actual alias. This is useful when $alias is modified by the database
   *   system, for example, when joining the same table more than once.
   * @param array $arguments
   *   An array of arguments to replace into the $condition of this join.
   */
  public function innerJoin($table, $alias = NULL, $condition = NULL, $arguments = array()) {
    $this->joins[$alias] = array(
      'type' => 'innerJoin',
      'table' => $table,
      'alias' => $alias,
      'condition' => $condition,
      'arguments' => $arguments,
    );
  }

  /**
   * Left Outer Join against another table in the database.
   *
   * When refering to the table, you wish to join up to, give full table name.
   * See user.entity_backend_search.inc.
   *
   * @param string $table
   *   The table against which to join.
   * @param string $alias
   *   The alias for the table. In most cases this should be the first letter
   *   of the table, or the first letter of each "word" in the table.
   * @param string $condition
   *   The condition on which to join this table. If the join requires values,
   *   this clause should use a named placeholder and the value or values to
   *   insert should be passed in the 4th parameter. For the first table joined
   *   on a query, this value is ignored as the first table is taken as the base
   *   table. The token %alias can be used in this string to be replaced with
   *   the actual alias. This is useful when $alias is modified by the database
   *   system, for example, when joining the same table more than once.
   * @param array $arguments
   *   An array of arguments to replace into the $condition of this join.
   */
  public function leftJoin($table, $alias = NULL, $condition = NULL, $arguments = array()) {
    $this->joins[$alias] = array(
      'type' => 'leftJoin',
      'table' => $table,
      'alias' => $alias,
      'condition' => $condition,
      'arguments' => $arguments,
    );
  }

  /**
   * Right Outer Join against another table in the database.
   *
   * When refering to the table, you wish to join up to, give full table name.
   * See user.entity_backend_search.inc.
   *
   * @param string $table
   *   The table against which to join.
   * @param string $alias
   *   The alias for the table. In most cases this should be the first letter
   *   of the table, or the first letter of each "word" in the table.
   * @param string $condition
   *   The condition on which to join this table. If the join requires values,
   *   this clause should use a named placeholder and the value or values to
   *   insert should be passed in the 4th parameter. For the first table joined
   *   on a query, this value is ignored as the first table is taken as the base
   *   table. The token %alias can be used in this string to be replaced with
   *   the actual alias. This is useful when $alias is modified by the database
   *   system, for example, when joining the same table more than once.
   * @param array $arguments
   *   An array of arguments to replace into the $condition of this join.
   */
  public function rightJoin($table, $alias = NULL, $condition = NULL, $arguments = array()) {
    $this->joins[$alias] = array(
      'type' => 'rightJoin',
      'table' => $table,
      'alias' => $alias,
      'condition' => $condition,
      'arguments' => $arguments,
    );
  }

  /**
   * Helper function: builds the most common conditional clauses.
   *
   * This method can take a variable number of parameters. If called with two
   * parameters, they are taken as $field and $value with $operator having a
   * value of IN if $value is an array and = otherwise.
   *
   * Do not use this method to test for NULL values. Instead, use
   * QueryConditionInterface::isNull() or QueryConditionInterface::isNotNull().
   *
   * @param string $field
   *   The name of the field to check. If you would like to add a more complex
   *   condition involving operators or functions, use where().
   * @param mixed $value
   *   The value to test the field against. In most cases, this is a scalar.
   *   For more complex options, it is an array. The meaning of each element in
   *   the array is dependent on the $operator.
   * @param string $operator
   *   The comparison operator, such as =, <, or >=. It also accepts more
   *   complex options such as IN, LIKE, or BETWEEN. Defaults to IN if $value is
   *   an array, and = otherwise.
   *
   * @see QueryConditionInterface::isNull()
   * @see QueryConditionInterface::isNotNull()
   */
  public function condition($field, $value = NULL, $operator = NULL) {
    $this->conditions[] = array(
      'field' => $field,
      'value' => $value,
      'operator' => $operator,
    );
  }

  /**
   * Extend the parent finishQuery method to enable joins and conditions.
   *
   * @param object $select_query
   *   The query object to add joins and conditions to.
   * @param string $id_key
   *   The key to get the entity id from.
   *
   * @return object
   *   The query object with the result set.
   */
  public function finishQuery($select_query, $id_key = 'entity_id') {

    // Force it to use database slaves, if any, as these queries might be very
    // demanding on the database.
    if (!empty($this->joins)) {
      // Add distinct() to make sure that 1->n results won't output duplicates.
      $select_query->distinct();

      foreach ($this->joins as $join) {
        switch ($join['type']) {
          case 'leftJoin':
            $select_query->innerJoin($join['table'], $join['alias'], $join['condition'], $join['arguments']);
            break;

          case 'rightJoin':
            $select_query->innerJoin($join['table'], $join['alias'], $join['condition'], $join['arguments']);
            break;

          case 'innerJoin':
            $select_query->innerJoin($join['table'], $join['alias'], $join['condition'], $join['arguments']);
            break;
        }
      }
    }

    foreach ($this->conditions as $condition) {
      $select_query->condition($condition['field'], $condition['value'], $condition['operator']);
    }

    // Do the actual query and return the query object containing the result
    // set.
    return parent::finishQuery($select_query, $id_key);
  }
}
